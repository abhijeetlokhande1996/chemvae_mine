from tensorflow import keras
import pandas as pd
import numpy as np


def main(df):
    smiles_arr = df["smiles"].tolist()
    smiles_arr = [s.strip() for s in smiles_arr]
    SMILE_LENGTH = len(smiles_arr)
    MAX_LENGTH = max([len(s) for s in smiles_arr])
    s = set()
    for item in smiles_arr:
        s = s.union(item)
    char_dict = {}
    for idx, c in enumerate(s):
        char_dict[c] = idx
    NCHARS = len(s)
    print("SMILE LENGTH: ", SMILE_LENGTH)
    print("MAX LENGTH: ", MAX_LENGTH)
    print("NCHARS: ", NCHARS)
    X = np.zeros((SMILE_LENGTH, MAX_LENGTH, NCHARS))
    for i, smile in enumerate(smiles_arr):
        for j, char in enumerate(smile):
            X[i, j, char_dict[char]] = 1
    print(X.shape)


if __name__ == '__main__':
    df = pd.read_csv("./250k_rndm_zinc_drugs_clean_3.csv")

    main(df)
